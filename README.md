<!--
SPDX-FileCopyrightText: 2022 Andrey Bienkowski <Andrey Bienkowski>

SPDX-License-Identifier: MIT
-->

Check that a soundpack for [C:DDA](https://cataclysmdda.org/) does not reference missing files.
Example issue: https://github.com/Fris0uman/CDDA-Soundpacks/pull/24

# Hacking

```sh
# 1. Install pre-commit hooks
pipx run pre-commit install
```
