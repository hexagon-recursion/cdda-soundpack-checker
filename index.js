// SPDX-FileCopyrightText: 2022 Andrey Bienkowski <Andrey Bienkowski>
//
// SPDX-License-Identifier: MIT

import yargs from "yargs";
import { hideBin } from "yargs/helpers";

const args = yargs(hideBin(process.argv))
  .command(
    "$0 <soundpack_txt>",
    "Check that a soundpack for C:DDA does not reference missing files"
  )
  .positional("soundpack_txt", {
    describe: "Path to soundpack.txt",
    type: "string",
  })
  .strict().argv;
console.log(args);
